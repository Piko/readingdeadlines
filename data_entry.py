import csv
from datetime import datetime

# TODO: Make https://xkcd.com/1179/ -compliant

MONTH = 2  # TODO: current month unten ist noch zu flexibel
monthstr = f"{MONTH:02d}"

DATA_PATH = "./data.csv"
BOOK_PATH = "./buecher_" + monthstr  + ".csv"



shorthands = []
books = {}

with open(BOOK_PATH, newline='') as csvfile:
    booksreader = csv.reader(csvfile, delimiter=',', quotechar='|')
    next(booksreader)
    for row in booksreader:  # row sind listen, datareader ist ein _csv.reader
        if len(row) == 5:
            title, persons, category, shorthand, pages_total = row
        if len(row) == 6:
            title, persons, category, shorthand, pages_total, unit = row
        books[shorthand] = (title, persons, category, pages_total)
        shorthands.append(shorthand)


bookname = 0
while bookname not in shorthands:
    print("Available shorthands:", ", ".join(shorthands))
    bookname = input("Enter the name of the book: ")
    if bookname in shorthands:
        break

    for shorthand in books:  # doesn't seem very elegant
        if bookname in books[shorthand]:
            bookname = shorthand  # FIXME: This doesn't work
            break
    if bookname in shorthands:
        break

    # add fuzzy matching
    for shorthand in books:
        if bookname[:2] == shorthand[:2]:
            if input("Did you mean " + shorthand + "? (y/n): ") == "y":
                bookname = shorthand
                break
    if bookname in shorthands:
        break

    print("Couldn't find the name in the database. Please retry:")
    # TODO: add possibility to add a book


# By this moment, bookname should contain the shorthand of one of the books in BOOK_PATH

with open(DATA_PATH, newline='') as datafile:
    datareader = csv.reader(datafile, delimiter=',')
    next(datareader)
    current_page = 0
    for row in datareader:
        if row[1] == bookname:
            current_page = int(row[2])

print("Current page:", current_page, "Last page:", books[bookname][3])


new_page = input("Enter page: ")
if new_page[0] == "+":
    new_page = current_page + int(new_page[1:])
else:
    while not new_page.isnumeric():
        new_page = input("Please enter a valid page number: ")

# todo better: with try/except


date_time = datetime.now().strftime("%d/%m/%Y %H:%M")


with open(DATA_PATH, mode='a') as data_file:
    data_writer = csv.writer(data_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    data_writer.writerow([date_time, bookname, new_page])