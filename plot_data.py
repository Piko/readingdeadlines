import csv
import matplotlib.pyplot as plt
import datetime as dt
import matplotlib.dates as mdates
from calendar import monthrange
from matplotlib.ticker import AutoMinorLocator, AutoLocator
import numpy as np

# TODO: lesedeadline-Plot: Add a factor for difficult books / music scores
# Todo: Show derivative!
# Todo: Maybe have light colors above the plot to show the pages that are yet to be read
# Todo: show which books are done, maybe with a black line?

MONTH = 4
YEAR = 2023
monthstr = f"{MONTH:02d}"

DATA_PATH = "./data.csv"
BOOK_PATH = "./buecher_" + monthstr + ".csv"

x = []
y = {}

page_total = 0
booknames = []
pages_list = []

with open(BOOK_PATH, newline='') as csvfile:
    datareader = csv.reader(csvfile, delimiter=',', quotechar='|')
    next(datareader)
    for row in datareader:  # row sind listen, datareader ist ein _csv.reader
        if row == []:
            continue
        y[row[3]] = []  # todo: verstehen: was passiert hier?
        booknames.append(row[0])
        pages_list.append(int(row[4]))
        page_total += int(row[4])

with open(DATA_PATH, newline='') as csvfile:
    datareader = csv.reader(csvfile, delimiter=',', quotechar='|')
    next(datareader)

    for row in datareader:  # row sind listen, datareader ist ein _csv.reader
        if row[0][3:5] == monthstr:

            datum, kuerzel, seite = row
            print("    processing", datum, kuerzel, seite)
            neu = dt.datetime.strptime(datum[:10], '%d/%m/%Y').date()
            if neu in x:
                # heute schon gelesen => das aktuelle datum ist schon in x.
                # => in y muss nur der letzte Eintrag von kuerzel verändert werden
                print("         datumskollision", datum)
                print("         bei kuerzel:", kuerzel, "bisherige Entwicklung:", y[kuerzel], "neue seite:", seite)
                y[kuerzel][-1] = int(seite)
                ...
            else:  # heute noch nicht gelesen => neues datum in x,
                x.append(neu)
                for k in y:  # bei jedem Kürzel wird eine Zahl angehängt
                    if k == kuerzel:  # beim richtigen Kürzel wird die neue Seitenzahl angehängt
                        y[k].append(int(seite))
                    elif not y[k]:  # bei den Büchern, die noch nicht gelesen wurden, wird 0 angehängt
                        y[k].append(0)
                    else:  # bei den anderen Büchern wurde nicht weitergelesen.
                        y[k].append(y[k][-1])

for i in range(len(x)):
    print(x[i], end="      ")
    for d in y:
        print(f'{y[d][i]:>5}', end="   ")   # f'{text:>5}' adds leading whitespace to make every string the same length
    print()

fig, ax = plt.subplots()

# Goal: horizontal line
ax.axhline(y=page_total, color='g', linestyle='--')  # , label="Gesamt")

# Progress: needs just two data points
date_first_of_month = dt.datetime(YEAR, MONTH, 1).date()
date_last_of_month = dt.datetime(YEAR, MONTH, monthrange(YEAR, MONTH)[1]).date()
x_progress = [date_first_of_month, date_last_of_month]
y_progress = [0, page_total]
ax.plot(x_progress, y_progress, "-r")


# Turn on the minor TICKS, which are required for the minor GRID
ax.minorticks_on()

ax.grid(which='minor', linestyle=':', linewidth='0.5')

plt.title('Reading deadlines ' + monthstr + "/" + str(YEAR))
plt.xlabel('Date', color='#1C2833')
plt.ylabel('Page total', color='#1C2833')
ax.grid()

# Only show minor gridlines once in between major gridlines.
ax.xaxis.set_minor_locator(AutoMinorLocator(1))
ax.yaxis.set_minor_locator(AutoMinorLocator(2))

for label in ax.get_xticklabels():
    label.set_fontsize(7)

plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%d.%m.%Y'))
plt.gca().xaxis.set_major_locator(mdates.DayLocator())
plt.gcf().autofmt_xdate()
for label in ax.xaxis.get_ticklabels()[::2]:
    label.set_visible(False)

# todo: make the area darker when last page is reached

y_data = [d for d in y.values()]
legend_lables = [f"{booknames[i]} ({pages_list[i]} S.)" for i in range(len(booknames))]
ax.stackplot(x, *y_data, labels=legend_lables)

# todo: add cumulative derivative as a smooth line

ax.legend(loc='upper left', fontsize=8)
plt.show()
